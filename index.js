const puppeteer = require('puppeteer');
const sleep = require('util').promisify(setTimeout);

(async () => {

  const browser = await puppeteer.launch({
    headless: false
  });

  if (!browser) {
    console.log('no browser');
    return;
  }

  const page = await browser.newPage();
  await page.goto('https://www.ocado.com');
  let title = await page.title();
  console.log('title    > ', title);

  while (title.match(/virtual queue/i)) {
    const cookies = await page.cookies();
    const aluid = cookies.find(cookie => cookie.name === 'aluid');
    console.log('aluid    > ', aluid.value);
    const buffer = new Buffer.from(aluid.value, 'base64');
    console.log('buffer   > ', buffer);
    const content = await page.content();
    const position = content.match(/<strong id="queuePosition">(\d+)</)[1];
    const queueLength = content.match(/<span id="queueLength">(\d+)</)[1];
    console.log(`position > ${position} / ${queueLength} `, new Date());
    await sleep(15000);
    title = await page.title();
  }

  console.log('title    > ', title);

  while (title.match(/online supermarket/i)) {
    const cookies = await page.cookies();
    const aluid = cookies.find(cookie => cookie.name === 'aluid');
    console.log('aluid    > ', aluid.value);
    const buffer = new Buffer.from(aluid.value, 'base64');
    console.log('buffer   > ', buffer);
    await sleep(180000 + (parseInt(Math.random() * 180000)));
    await page.reload();
    console.log('reloaded > ', new Date());
    title = await page.title();
  }

  console.log('title    > ', title);
  console.log('looks like we got logged out');
  return;
})();